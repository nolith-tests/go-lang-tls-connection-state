package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"net/http"
	"os"
	"runtime"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		dumpConnectionState(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
}

func dumpConnectionState(url string) {
	fmt.Println("URL", url, "with", runtime.Version())
	r, err := http.Head(url)
	if err != nil {
		panic(err)
	}

	fmt.Println("VerifiedChains len", len(r.TLS.VerifiedChains))
	for i, verifiedChain := range r.TLS.VerifiedChains {
		fmt.Println("Chain #", i)
		for j, certificate := range verifiedChain {
			signature := hex.EncodeToString(certificate.Signature)
			fmt.Println("[", j, "] =>", certificate.Subject.CommonName, signature)
		}
	}
}
